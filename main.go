package main

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"os"
)

func main() {
	// Parse args with path to config file
	argsProgram := os.Args[1:]
	if len(argsProgram) != 1 {
		fmt.Fprintf(os.Stderr, "Not valid arg\n")
		os.Exit(1)
	}

	// Read config file
	config := make(map[string]string)
	err := readConfig(argsProgram[0], config)
	exitOnError(err)

	// Initial Logging
	if logfile, ok := config["application_logfile"]; ok {
		f, err := os.OpenFile(logfile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
		defer f.Close()
		exitOnError(err)
		log.SetOutput(f)
	} else {
		fmt.Fprintf(os.Stderr, "Error: not found main_migration in application config\n")
		os.Exit(1)
	}

	// Create configuration for databases
	var mainDbConf DatabaseConfig
	var usersDbConf DatabaseConfig

	err = mainDbConf.initialConfig("main", config)
	exitOnError(err)

	err = usersDbConf.initialConfig("users", config)
	exitOnError(err)

	// Create databases connections
	mainDbConnect, err := mainDbConf.initDatabase()
	exitOnError(err)
	defer mainDbConnect.Close()

	usersDbConnect, err := usersDbConf.initDatabase()
	exitOnError(err)
	defer usersDbConnect.Close()

	// load sql from files and do program preparation
	if migration, ok := config["application_main_migration"]; ok {
		loadMigration(migration, mainDbConnect)
	} else {
		fmt.Fprintf(os.Stderr, "Error: not found main_migration in application config\n")
		os.Exit(1)
	}

	if migration, ok := config["application_users_migration"]; ok {
		loadMigration(migration, usersDbConnect)
	} else {
		fmt.Fprintf(os.Stderr, "Error: not found users_migration in application config\n")
		os.Exit(1)
	}

	// Read data from users DB and write to main DB and return user map
	userContainer := transportUsersAcrossDBs(usersDbConnect, mainDbConnect)

	// Delete users, they are not active from user map.
	userContainer.DeleteNotActiveUser()
}
