package main

import (
	"database/sql"
	"errors"
	"fmt"
	"time"
)

type User struct {
	id         int
	l_name     string
	f_name     string
	age        int
	gender     string
	marital    bool
	last_login int64
}

func (user *User) String() string {
	return fmt.Sprintf(
		"%d %s %s %d %s %t %d\n",
		user.id,
		user.f_name,
		user.l_name,
		user.age,
		user.gender,
		user.marital,
		user.last_login)
}

func (user *User) SetFromRows(rows *sql.Rows) {
	var gender int
	var marital int
	var last_login string
	err := rows.Scan(&user.id, &user.l_name, &user.f_name, &user.age, &gender, &marital, &last_login)
	user.setGender(gender)
	user.setMarital(marital)
	user.setLastLogin(last_login)
	exitOnError(err)
}

func (user *User) setLastLogin(timeString string) {
	if len(timeString) > 0 {
		user.last_login = stringToUnixTimestamp(timeString)
	} else {
		user.last_login = 0
	}
}

func (user *User) setMarital(marital int) {
	user.marital = !(marital == 0)
}

func (user *User) setGender(gender int) {
	switch gender {
	case 0:
		user.gender = "other"
	case 1:
		user.gender = "m"
	case 2:
		user.gender = "f"
	default:
		exitOnError(errors.New("Unexpected value in gender field"))
	}
}

func (user *User) getMarital() int {
	if user.marital {
		return 1
	}
	return 0
}

func (user *User) getGender() int {
	switch user.gender {
	case "m":
		return 1
	case "f":
		return 2
	default:
		return 0
	}
}

func stringToUnixTimestamp(timeString string) int64 {
	layout := "02.01.2006 15:04"
	t, err := time.Parse(layout, timeString)
	exitOnError(err)
	return t.Unix()
}

func writeUserToMain(connect *sql.DB, user User) {
	sqlRequest := "INSERT INTO users (`id`, `l_name`, `f_name`, `age`, `gender`, `marital`, `last_login` ) VALUES (?, ?, ?, ?, ?, ?, ?)"
	_, err := connect.Exec(sqlRequest,
		user.id,
		user.l_name,
		user.f_name,
		user.age,
		user.getGender(),
		user.getMarital(),
		user.last_login,
	)
	exitOnError(err)
}

func transportUsersAcrossDBs(userConn *sql.DB, mainConn *sql.DB) *UserContainer {
	sqlRequest := `
		SELECT users.*, users_data.age, users_data.gender, users_data.marital, users_data.last_login
		FROM users
		LEFT JOIN users_data ON users.id = users_data.id`
	rows, err := userConn.Query(sqlRequest)
	exitOnError(err)

	// Create shared map with users

	userContainer := NewUserContainer()

	for rows.Next() {
		user := User{}
		user.SetFromRows(rows)

		// Output data to stdout
		fmt.Printf(user.String())

		// Write user to shared map
		userContainer.Put(user.id, user)

		// Write data to Main DB
		writeUserToMain(mainConn, user)
	}
	rows.Close()
	return userContainer

}
