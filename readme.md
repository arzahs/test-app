# Test for Golang

Example usage:

1. Create config file and change config
```bash
    $ cp config.ini.example config.ini
```
2. Build program
```bash
    $ go build
```
3. Program usage example:
```bash
    $ test-app ./config.ini
```




