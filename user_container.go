package main

import (
	"errors"
	"log"
	"sync"
)

// Struct for realization SharedMap
// Use Mutex for safe access to map
type UserContainer struct {
	sync.RWMutex
	values map[int]User
}

func NewUserContainer() *UserContainer {
	return &UserContainer{
		values: make(map[int]User),
	}
}

// Safe "get" operation for UserContainer
func (t *UserContainer) Get(key int) (User, error) {
	t.RLock()
	defer t.RUnlock()
	value, ok := t.values[key]
	if !ok {
		return User{}, errors.New("Invalid key")
	}
	return value, nil
}

// Safe "put" operation for UserContainer
func (t *UserContainer) Put(key int, value User) {
	t.Lock()
	defer t.Unlock()
	t.values[key] = value
}

// Not safe "delete" operation for UserContainer
func (t *UserContainer) Delete(key int) {
	delete(t.values, key)
}

// Method for delete all not active user
// users with last_login is 0 aren`t active
func (t *UserContainer) DeleteNotActiveUser() {
	t.Lock()
	defer t.Unlock()
	for key, value := range t.values {
		if value.last_login == 0 {
			// Login deleted user
			log.Printf("Delete not active user = %s\n", value.String())
			t.Delete(key)
		}
	}
}
