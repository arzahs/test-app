
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `l_name` varchar(255) NOT NULL DEFAULT '',
  `f_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'John', 'Doe');
INSERT INTO `users` VALUES ('2', 'Joshua', 'Star');
INSERT INTO `users` VALUES ('3', 'Jane', 'Sith');
INSERT INTO `users` VALUES ('4', 'Robert', 'Milson');
INSERT INTO `users` VALUES ('5', 'Elisabeth', 'Morth');
INSERT INTO `users` VALUES ('6', 'Bary', 'Sorm');
INSERT INTO `users` VALUES ('7', 'Mary', 'Douson');
INSERT INTO `users` VALUES ('8', 'Sarah', 'Connor');
INSERT INTO `users` VALUES ('9', 'Alistar', 'Tampler');
INSERT INTO `users` VALUES ('10', 'Veronica', 'Ingil');
INSERT INTO `users` VALUES ('11', 'Martin', 'Mulen');
INSERT INTO `users` VALUES ('12', 'Monica', 'Doust');
INSERT INTO `users` VALUES ('13', 'Michael', 'Kurst');

-- ----------------------------
-- Table structure for `users_data`
-- ----------------------------
DROP TABLE IF EXISTS `users_data`;
CREATE TABLE `users_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age` int(11) NOT NULL DEFAULT '0',
  `gender` tinyint(4) NOT NULL DEFAULT '0',
  `marital` tinyint(4) NOT NULL DEFAULT '0',
  `last_login` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users_data
-- ----------------------------
INSERT INTO `users_data` VALUES ('1', '35', '1', '1', '14.05.2016 12:37');
INSERT INTO `users_data` VALUES ('2', '17', '1', '0', '12.05.2016 17:30');
INSERT INTO `users_data` VALUES ('3', '20', '2', '1', '02.05.2016 07:22');
INSERT INTO `users_data` VALUES ('4', '44', '1', '1', '10.04.2016 10:00');
INSERT INTO `users_data` VALUES ('5', '19', '2', '1', '14.04.2016 17:31');
INSERT INTO `users_data` VALUES ('6', '20', '1', '0', '11.01.2016 12:27');
INSERT INTO `users_data` VALUES ('7', '33', '2', '1', '');
INSERT INTO `users_data` VALUES ('8', '28', '2', '1', '02.03.2014 22:31');
INSERT INTO `users_data` VALUES ('9', '30', '1', '1', '12.01.2016 17:30');
INSERT INTO `users_data` VALUES ('10', '22', '2', '0', '22.04.2016 12:55');
INSERT INTO `users_data` VALUES ('11', '12', '1', '0', '18.06.2015 14:00');
INSERT INTO `users_data` VALUES ('12', '76', '2', '1', '');
INSERT INTO `users_data` VALUES ('13', '27', '1', '0', '07.03.2015 12:12');
