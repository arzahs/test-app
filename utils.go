package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

// Function for simple errors handling
func exitOnError(err error) {
	if err != nil {
		log.Fatal(err)
		fmt.Fprintf(os.Stderr, "Error: %v\n", err)
		os.Exit(1)
	}
}

// Function that load SQL file from path
// And execute all commands from file
func loadMigration(pathToMigration string, db *sql.DB) {
	initialRequestMainDB, err := getSqlRequestsFromFile(pathToMigration)
	exitOnError(err)
	for _, request := range initialRequestMainDB {
		// check request string
		if len(strings.TrimSpace(request)) > 0 {
			// exec sql in DB
			_, err := db.Exec(request)
			exitOnError(err)
		}
	}
}

// Read sql file, and split it to request strings
func getSqlRequestsFromFile(pathToFile string) ([]string, error) {
	f, err := os.Open(pathToFile)
	defer f.Close()
	if err != nil {
		return nil, err
	}
	file, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	requests := strings.Split(string(file), ";")
	return requests, nil

}
