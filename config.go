package main

import (
	"database/sql"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
)

// Struct for wrapping databases settings
type DatabaseConfig struct {
	Name     string
	Host     string
	Port     string
	Password string
	Username string
}

// Method that initial DatabaseConfig from application config
func (conf *DatabaseConfig) initialConfig(dbName string, config map[string]string) error {
	if name, err := config[dbName+"_name"]; !err {
		return errors.New("Has not name " + dbName + " in config")
	} else {
		conf.Name = name
	}

	if host, err := config[dbName+"_host"]; !err {
		return errors.New("Has not host " + dbName + " in config")
	} else {
		conf.Host = host
	}

	if password, err := config[dbName+"_password"]; !err {
		return errors.New("Has not password " + dbName + " in config")
	} else {
		conf.Password = password
	}

	if username, err := config[dbName+"_username"]; !err {
		return errors.New("Has not username " + dbName + " in config")
	} else {
		conf.Username = username
	}

	if port, err := config[dbName+"_port"]; !err {
		conf.Port = "3306"
	} else {
		conf.Port = port
	}

	return nil
}

// Method that return DB connection from config
func (conf *DatabaseConfig) initDatabase() (*sql.DB, error) {
	sourceNameString := conf.Username + ":" + conf.Password + "@(" + conf.Host + ":" + conf.Port + ")/" + conf.Name + "?charset=utf8"
	fmt.Printf("%v\n", sourceNameString)
	db, err := sql.Open("mysql", sourceNameString)

	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}

// Simple .ini config reader,
// that initial application config map
func readConfig(path string, config map[string]string) error {
	var re *regexp.Regexp
	re, err := regexp.Compile("[#].*\\n|\\s+\\n|\\S+[=]|.*\n")
	if err != nil {
		return err
	}

	f, err := os.Open(path)
	if err != nil {
		return err
	}

	file, err := ioutil.ReadAll(f)
	if err != nil {
		return err
	}

	str := string(file)

	if !strings.HasSuffix(str, "\n") {
		str += "\n"
	}

	strlist := re.FindAllString(str, -1)
	sectionName := ""
	for i := 0; i < len(strlist); {
		if strings.HasPrefix(strlist[i], "#") {
			i++
		} else if strings.HasPrefix(strlist[i], "[") {
			sectionName = strings.ToLower(strlist[i])[1 : len(strlist[i])-2]
			i++
		} else if strings.HasSuffix(strlist[i], "=") {
			key := strings.ToLower(strlist[i])[0 : len(strlist[i])-1]
			i++
			if strings.HasSuffix(strlist[i], "\n") {
				val := strlist[i][0 : len(strlist[i])-1]
				if strings.HasSuffix(val, "\r") {
					val = val[0 : len(val)-1]
				}
				i++
				config[sectionName+"_"+key] = val
			}
		} else if strings.Index(" \t\r\n", strlist[i][0:1]) > -1 {
			i++
		} else {
			return errors.New("Unable to process line in cfg file containing " + strlist[i])
		}
	}

	return nil
}
